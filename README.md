Gardener-Bot
============

This is a simple sketch designed to run an Arduino (Uno?) equiped with the equivalent of: 

*  Bluefruit EZLink shield (for remote controll and telemetry)
*  LCD shield (with buttons for local control)
*  motor driver shield (for the water pump)
*  Sensor or IO shield (for sensors)

This is for prototype purposes only.

See also: TBD Gardener-Master, a django based app intended for a Raspberry Pi that will manage one or more Gardner-Bots from a web interface.


