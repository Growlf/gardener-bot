#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <Adafruit_RGBLCDShield.h>

#define VERSION "1.0"

#define WHITE 0x7

#define SOIL 0x1
#define AIR 0x2
#define TEMP_MIN 0x4
#define TEMP_MAX 0x8

Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
int last_update = millis();
char* modes[2] = {"  Auto","Manual"}; 
uint8_t mode=0;
int setting = 0;
int num_settings = 5;
char* settings_prompt[] = {"Soil", "Air", "Temp-", "Temp+", "Net"};
int settings[] = {50,70,50,90,0};

//////////////////////////////////////////////////////////////

// ------------ main initialization
void setup() {
  Serial.begin(9600);

  // set up the LCD
  lcd.begin(16, 2);
  lcd.setBacklight(WHITE);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Go Go Gardener!!");
  lcd.setCursor(0,1);
  lcd.print("U/D-Menu L/R-val");
}

// ------------ main loop

void loop() {
  boolean update = false;
  String strStatus;
  String strPrompt;

  // Serial input
  if (Serial.available()) {
    uint8_t serdata = Serial.read();
    
    // Auto mode "a" or "m" Manual
    if (97==serdata){
      mode = 0;
      update = true;
    } else if (109==serdata){
      mode = 1;
      update = true;
    }

    // Set "s" or "q" Query values
    else if (115==serdata){
      update = true;
      strStatus=String("(S)et");
    } else if (113==serdata){
      update = true;
      strStatus = String("(Q)uery");
    }

    // Networking "n"
    else if (110==serdata){
      strStatus=String("(N)et");
    } 

    // All else
    else {
      update = true;
      strStatus=String("UNKNOWN");
      strPrompt=String(serdata);
    }
  }

  // Button input
  uint8_t buttons = lcd.readButtons();
  if (buttons) {
    
    // ANY keypress should turn on the display
    update = true;
    
    // Selecting the setting or command
    if (buttons & BUTTON_UP) {
      setting = setting + 1;
      // debounce the buttons for slow fingers
      delay(300);
    }
    if (buttons & BUTTON_DOWN) {
      setting = setting - 1;
      // debounce the buttons for slow fingers
      delay(300);    
    }

    // Modify the value or select the subcommand
    if (buttons & BUTTON_LEFT) {
      settings[setting] = settings[setting] - 1;
    }
    if (buttons & BUTTON_RIGHT) {
      settings[setting] = settings[setting] + 1;
    }

    // Update the value or execute the command
    if (buttons & BUTTON_SELECT) {
      strPrompt=String("SET");
    }

    if (setting >= num_settings) setting = 0;
    if (setting < 0) setting = num_settings;
    strStatus = settings_prompt[setting];
    strPrompt = String(settings[setting]);

  }
  
  // Update the LCD as needed
  if (update) {
    String strSettings;

    // Initialize    
    lcd.setBacklight(WHITE);
    lcd.clear();

    // Display Command/Status
    lcd.setCursor(0,0);
    lcd.print(strStatus);

    // Display Operating Mode
    lcd.setCursor(9,0);
    lcd.print(modes[mode]);

    // Display prompt/value
    lcd.setCursor(0,1);
    lcd.print(strPrompt);

    // Update the server or console
    strSettings = String("ver=");
    strSettings = strSettings + VERSION;
    for(int s=0; s < num_settings; s++) {
      strSettings = strSettings + ":" + settings_prompt[s] + "=" + settings[s]; 
    }
    Serial.println(strSettings + ":Mode=" + mode + ":Last=" + (millis() - last_update));

    // Rest the screen dimmer
    last_update = millis();
  } 
  
  // Shut screen off if apropriate
  if ((last_update + 5000) < millis()) {
    lcd.setBacklight(0);
  }
}


